#include <stdio.h>
#include <stdlib.h>
#include <math.h>

long rand_48(long x0){
    long r = ((((25214903917*x0) +11)) % ((long)pow(2,48)));
    return r;
}

long exo9_gen(long val1, long val2){
    long y1 = val1 << 16;
    while((rand_48(y1) >> 16) != val2){
        y1++;
    }
    long y00 =0;
    long y01 =0;
    while( ((rand_48(y00) >> 16) != val1 ) || ( (rand_48(y01) >> 16) != val1)) {
        y00++;
     y01--;
    }
    
   if((rand_48(y00) >> 16) == val1){
        return (y00 >> 16);

    }else if((rand_48(y01) >> 16) == val1){
        return ((y01) >> 16);
    }
    return -1;
}

int main(){  
    long r1 = rand_48(763);
    long r2 = rand_48(r1);
    long r3 = rand_48(r2);

    long resultat = exo9_gen(r2 >> 16,r3 >> 16);
    printf("\n v0 = %ld", resultat);
    
    return 0;
}
